library performance_indicator;

import 'package:flutter/cupertino.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/model/speed_details/speed_details.dart';
import 'package:performance_indicator/src/repo/db/hive_db.dart';
import 'package:performance_indicator/src/services/color_service.dart';
import 'package:performance_indicator/src/services/cron_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/services/speed_service.dart';
import 'package:performance_indicator/src/widgets/dialog/option/dialog_option_details.dart';
import 'package:performance_indicator/src/widgets/dialog/quality_preference_dialog.dart';
import 'package:performance_indicator/src/widgets/pulsing_button/pulsing_button.dart';

class PerformanceIndicatorWidget extends StatefulWidget {
  const PerformanceIndicatorWidget._({
    required this.width,
    required this.dialogOptions,
    required this.checkUri,
    required this.badResponseTime,
    required this.title,
  });

  final double width;
  final List<DialogOptionDetails> dialogOptions;
  final Uri checkUri;
  final int badResponseTime;
  final String title;

  static custom({
    required double width,
    required List<DialogOptionDetails> dialogOptions,
    int? badResponseTimeInMs,
    Uri? checkUrl,
    String? title,
  }) {
    return PerformanceIndicatorWidget._(
      width: width,
      badResponseTime: badResponseTimeInMs ?? 500,
      dialogOptions: dialogOptions,
      checkUri: checkUrl ?? SpeedService.checkUri,
      title: QualityPreferenceDialog.defaultTitle,
    );
  }

  static quality({
    required double width,
    int? badResponseTimeInMs,
    Uri? checkUri,
    String? title,
  }) {
    return PerformanceIndicatorWidget._(
      width: width,
      badResponseTime: badResponseTimeInMs ?? 500,
      dialogOptions: QualityPreferenceDialog.defaultOptions(),
      checkUri: checkUri ?? SpeedService.checkUri,
      title: title ?? QualityPreferenceDialog.defaultTitle,
    );
  }

  @override
  State<PerformanceIndicatorWidget> createState() =>
      _PerformanceIndicatorWidgetState();
}

class _PerformanceIndicatorWidgetState
    extends State<PerformanceIndicatorWidget> {
  late Future future;

  getData() async {
    await CronService().init();

    await HiveDB.instance();

    SpeedService.setBadResponseTime(widget.badResponseTime);
    SpeedService.setCheckUri(widget.checkUri);

    final speedService = await SpeedService.instance();
    final speedDetails = speedService.getLastCheckedSpeed();

    if (speedDetails != null) {
      final checkDateTime = DateTime.fromMillisecondsSinceEpoch(
        speedDetails.timestamp,
      );

      final maxAllowedTimeToCheck = DateTime.now().subtract(
        const Duration(seconds: 20),
      );

      if (checkDateTime.isBefore(maxAllowedTimeToCheck)) {
        await speedService.checkCurrentSpeed(
          timeoutMs: SpeedService.badResponseTime * 2,
        );
      }
    } else {
      await speedService.checkCurrentSpeed(
        timeoutMs: SpeedService.badResponseTime * 2,
      );
    }
  }

  @override
  void initState() {
    future = getData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: future,
        builder: (ctx, snapshot) {
          return snapshot.connectionState == ConnectionState.done
              ? ValueListenableBuilder(
                  valueListenable:
                      Hive.box<SpeedDetails>(SpeedService.dbName).listenable(),
                  builder: (context, Box box, w) {
                    final double speed =
                        box.isNotEmpty ? box.values.last.speedQuality : 0;

                    final selectedQuality = box.isNotEmpty
                        ? box.values.last.qualityPreference
                        : QualityPreference.high;

                    return PulsingButton(
                      buttonWidth: widget.width,
                      buttonColor: _getColor(speed, selectedQuality),
                      onTap: () => onOpenSpeedDialog(
                        context,
                        selectedQuality: selectedQuality,
                      ),
                    );
                  })
              : Container();
        });
  }

  onOpenSpeedDialog(
    BuildContext context, {
    required QualityPreference selectedQuality,
  }) async {
    final performanceService = await PerformanceService.instance();
    final selectedQuality = performanceService.getQualityPreference();

    showCupertinoDialog(
      barrierDismissible: true,
      context: context,
      builder: (ctx) => QualityPreferenceDialog(
        title: widget.title,
        withPaddingRight: false,
        withDivider: false,
        dialogOptions: widget.dialogOptions,
        closeAction: QualityPreferenceDialog.closeOption(selectedQuality),
      ),
    );
  }

  Color _getColor(double speed, QualityPreference selectedQuality) {
    return SpeedService.isActionRequired(speed, selectedQuality)
        ? ColorService.accentPrimary
        : ColorService.selectedPrimary;
  }
}
