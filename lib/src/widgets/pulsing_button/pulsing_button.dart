import 'package:flutter/material.dart';
import 'package:performance_indicator/src/widgets/pulsing_button/sprite_painter.dart';

class PulsingButton extends StatefulWidget {
  static const double buttonDefaultWidth = 100;

  const PulsingButton({
    Key? key,
    required this.buttonColor,
    required this.onTap,
    this.buttonWidth = buttonDefaultWidth,
  }) : super(key: key);

  final Color buttonColor;
  final Function() onTap;
  final double buttonWidth;

  @override
  PulsingButtonState createState() => PulsingButtonState();
}

class PulsingButtonState extends State<PulsingButton>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
    );

    _startAnimation();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  _startAnimation() {
    _controller
      ..stop()
      ..reset()
      ..repeat(period: const Duration(seconds: 1));
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: GestureDetector(
        onTap: widget.onTap,
        child: AnimatedBuilder(
          animation: _controller,
          child: CustomPaint(
            painter: SpritePainter(
              animation: _controller,
              buttonColor: widget.buttonColor,
            ),
            child: SizedBox(
              height: widget.buttonWidth,
              width: widget.buttonWidth,
            ),
          ),
          builder: (context, child) {
            return child ?? Container();
          },
        ),
      ),
    );
  }
}
