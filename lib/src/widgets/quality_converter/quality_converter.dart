import 'package:flutter/cupertino.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:performance_indicator/src/repo/db/hive_db.dart';
import 'package:performance_indicator/src/services/color_service.dart';
import 'package:performance_indicator/src/services/content_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/widgets/dialog/quality_converter_dialog.dart';
import 'package:performance_indicator/src/widgets/pulsing_button/pulsing_button.dart';

class QualityConverter extends StatefulWidget {
  const QualityConverter._({
    Key? key,
    required this.width,
    required this.contentId,
    required this.title,
    this.onChangedQuality,
  }) : super(key: key);

  final double width;
  final String contentId;
  final String title;
  final Function(String, QualityPreference)? onChangedQuality;

  static embeddable({
    required double width,
    required String contentId,
    String? title,
    Function(String, QualityPreference)? onChangedQuality,
  }) {
    return QualityConverter._(
      width: width,
      contentId: contentId,
      title: title ?? QualityConverterDialog.defaultTitle,
      onChangedQuality: onChangedQuality,
    );
  }

  @override
  State<QualityConverter> createState() => _QualityConverterState();
}

class _QualityConverterState extends State<QualityConverter> {
  late Future<void> future;

  Future<void> getData() async {
    await HiveDB.instance();
  }

  @override
  void initState() {
    future = getData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: future,
        builder: (context, snapshot) {
          return snapshot.connectionState == ConnectionState.done
              ? SizedBox(
                  height: widget.width,
                  width: widget.width,
                  child: ValueListenableBuilder(
                    valueListenable:
                        Hive.box<ContentRecording>(ContentService.dbName)
                            .listenable(),
                    builder: (context, Box contentBox, child) {
                      final content = contentBox.values
                          .where((element) =>
                              element.contentId == widget.contentId)
                          .toList();

                      QualityPreference? contentQuality;

                      if (content.isNotEmpty) {
                        contentQuality = content.first.qualityPreference;
                      }

                      return ValueListenableBuilder(
                        valueListenable:
                            Hive.box(PerformanceService.dbName).listenable(),
                        builder: (context, Box qualityPreferenceBox, child) {
                          final quality = PerformanceService.getQualityFromBox(
                            qualityPreferenceBox,
                          );

                          return PulsingButton(
                            buttonWidth: widget.width,
                            buttonColor: _buttonColor(
                              contentQuality,
                              quality,
                            ),
                            onTap: () => onOpenConvertDialog(
                              context,
                              contentQuality: contentQuality,
                              currentQuality: quality,
                            ),
                          );
                        },
                      );
                    },
                  ))
              : Container();
        });
  }

  onOpenConvertDialog(
    BuildContext context, {
    QualityPreference? contentQuality,
    QualityPreference? currentQuality,
  }) async {
    showCupertinoDialog(
      barrierDismissible: true,
      context: context,
      builder: (ctx) => currentQuality != contentQuality
          ? QualityConverterDialog(
              contentId: widget.contentId,
              title: widget.title,
              withPaddingRight: false,
              withDivider: true,
              dialogOptions: QualityConverterDialog.defaultOptions(
                currentQuality: currentQuality,
                contentQuality: contentQuality,
              ),
              closeAction: QualityConverterDialog.closeOption(),
              onChangedQuality: widget.onChangedQuality,
            )
          : QualityConverterDialog(
              contentId: widget.contentId,
              title: 'No action required',
              withPaddingRight: true,
              withDivider: false,
              dialogOptions: [
                DialogOptionDetails(
                  preference: contentQuality ?? QualityPreference.low,
                  title: 'reload',
                  description: [
                    contentQuality.toString().split('.').last,
                  ],
                  icon: MdiIcons.reload,
                  iconColor: ColorService.fontPrimary,
                  height:
                      QualityConverterDialog.defaultHeightDialogOption * 0.75,
                  adjustFontToIconColor: true,
                ),
              ],
              closeAction: QualityConverterDialog.closeOption(),
              onChangedQuality: widget.onChangedQuality,
            ),
    );
  }

  Color _buttonColor(
    QualityPreference? contentQuality,
    QualityPreference? quality,
  ) {
    bool isActionRequired = false;

    if (quality != null && contentQuality != null) {
      isActionRequired = contentQuality != quality;
    }

    return isActionRequired
        ? ColorService.accentPrimary
        : ColorService.selectedPrimary;
  }
}
