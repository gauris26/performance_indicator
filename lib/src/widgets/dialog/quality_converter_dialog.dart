import 'package:flutter/cupertino.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:performance_indicator/performance_indicator.dart';
import 'package:performance_indicator/src/services/color_service.dart';
import 'package:performance_indicator/src/services/content_service.dart';
import 'package:performance_indicator/src/widgets/dialog/option/dialog_option.dart';

class QualityConverterDialog extends StatelessWidget {
  static const String defaultTitle = 'Unexpected quality';
  static const double defaultHeightDialogOption = 100;

  const QualityConverterDialog({
    Key? key,
    required this.contentId,
    required this.title,
    required this.dialogOptions,
    required this.closeAction,
    this.withDivider = false,
    this.withPaddingRight = true,
    this.onChangedQuality,
  }) : super(key: key);

  final String contentId;
  final String title;
  final List<DialogOptionDetails> dialogOptions;
  final DialogOptionDetails closeAction;
  final bool withDivider;
  final bool withPaddingRight;
  final Function(String, QualityPreference)? onChangedQuality;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: Text(
        title,
        style: _titleTextStyle(),
      ),
      content: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: dialogOptions.map((details) {
          return DialogOption(
            details: details,
            height: details.height * 0.75,
            iconColor: details.iconColor,
            withDivider: withDivider,
            withPaddingRight: withPaddingRight,
            adjustColorOfSelected: false,
            fontColor: details.adjustFontToIconColor ? details.iconColor : null,
            onTap: () => _onTapOption(context, details),
            last: dialogOptions.last == details,
          );
        }).toList(),
      ),
      actions: [
        CupertinoDialogAction(
          onPressed: () => _onCloseDialog(context),
          child: Text(
            closeAction.title,
            style: _closeTextStyle(),
          ),
        )
      ],
    );
  }

  Future<void> _onTapOption(
    BuildContext context,
    DialogOptionDetails option,
  ) async {
    Navigator.of(context).pop();

    if (option.preference != null) {
      final contentService = await ContentService.instance();

      await contentService.updateQualityPreference(
        contentId,
        option.preference!,
      );

      if (onChangedQuality != null) {
        onChangedQuality!(contentId, option.preference!);
      }
    }
  }

  _onCloseDialog(BuildContext context) {
    Navigator.of(context).pop();
  }

  TextStyle _titleTextStyle() => const TextStyle(
        fontSize: 17,
        color: ColorService.fontPrimary,
      );

  TextStyle _closeTextStyle() => const TextStyle(
        fontSize: 15,
        color: ColorService.accentSecondary,
      );

  static DialogOptionDetails closeOption() => DialogOptionDetails(
        title: 'cancel',
        description: [],
        icon: MdiIcons.close,
        iconColor: ColorService.accentSecondary,
        height: defaultHeightDialogOption,
        preference: QualityPreference.high,
      );

  static List<DialogOptionDetails> defaultOptions({
    QualityPreference? currentQuality,
    QualityPreference? contentQuality,
  }) =>
      [
        DialogOptionDetails(
          preference: currentQuality ?? QualityPreference.high,
          title:
              'adjust to ${currentQuality.toString().split('.').last} quality',
          description: [],
          icon: MdiIcons.wifiSettings,
          iconColor: ColorService.accentPrimary,
          height: defaultHeightDialogOption,
          adjustFontToIconColor: true,
        ),
        DialogOptionDetails(
          preference: contentQuality ?? QualityPreference.low,
          title:
              'keep quality at ${contentQuality.toString().split('.').last} ',
          description: [],
          icon: MdiIcons.emoticonHappyOutline,
          iconColor: ColorService.selectedPrimary,
          height: defaultHeightDialogOption,
          adjustFontToIconColor: true,
        ),
      ];
}
