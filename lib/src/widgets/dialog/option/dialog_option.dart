import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:performance_indicator/src/services/color_service.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/widgets/dialog/option/dialog_option_details.dart';

class DialogOption extends StatelessWidget {
  const DialogOption({
    Key? key,
    required this.details,
    required this.height,
    required this.onTap,
    required this.last,
    this.withDivider = true,
    this.withPaddingRight = false,
    this.adjustColorOfSelected = true,
    this.fontColor = ColorService.fontPrimary,
    this.iconColor = ColorService.fontPrimary,
  }) : super(key: key);

  final DialogOptionDetails details;
  final double height;
  final Function() onTap;
  final bool last;
  final bool withDivider;
  final bool withPaddingRight;
  final bool adjustColorOfSelected;
  final Color? fontColor;
  final Color iconColor;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: Hive.box(PerformanceService.dbName).listenable(),
      builder: (context, Box box, widget) {
        final selectedQuality = PerformanceService.getQualityFromBox(box);
        late bool selected = false;

        if (selectedQuality != null) {
          selected = details.preference == selectedQuality;
        }

        return Column(
          children: [
            GestureDetector(
              onTap: onTap,
              child: Container(
                height: height,
                color: Colors.transparent,
                child: StaggeredGrid.count(
                  crossAxisCount: 14,
                  children: [
                    StaggeredGridTile.extent(
                      crossAxisCellCount: 1,
                      mainAxisExtent: height,
                      child: Container(),
                    ),
                    StaggeredGridTile.extent(
                      crossAxisCellCount: 2,
                      mainAxisExtent: height,
                      child: Container(
                        alignment: withDivider || details.description.isNotEmpty
                            ? Alignment.centerRight
                            : Alignment.bottomRight,
                        child: Icon(
                          details.icon,
                          color: adjustColorOfSelected && selected
                              ? ColorService.accentPrimary
                              : iconColor,
                          size: 20,
                        ),
                      ),
                    ),
                    StaggeredGridTile.extent(
                      crossAxisCellCount: withPaddingRight ? 8 : 11,
                      mainAxisExtent: height,
                      child: Container(
                        alignment: withDivider || details.description.isNotEmpty
                            ? Alignment.center
                            : Alignment.bottomCenter,
                        child: Column(
                          mainAxisAlignment:
                              withDivider || details.description.isNotEmpty
                                  ? MainAxisAlignment.center
                                  : MainAxisAlignment.end,
                          children: [
                            Text(
                              details.title,
                              style: _titleTextStyle(selected),
                            ),
                            details.description.isNotEmpty
                                ? const SizedBox(height: 2)
                                : Container(),
                            ...details.description
                                .map(
                                  (descriptionString) => Container(
                                    margin: const EdgeInsets.symmetric(
                                      vertical: 1,
                                    ),
                                    child: Text(
                                      descriptionString,
                                      style: _descriptionTextStyle(
                                        descriptionString,
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                          ],
                        ),
                      ),
                    ),
                    withPaddingRight
                        ? StaggeredGridTile.extent(
                            crossAxisCellCount: 3,
                            mainAxisExtent: height,
                            child: Container(),
                          )
                        : Container(),
                  ],
                ),
              ),
            ),
            ...(!last && withDivider)
                ? [
                    Container(
                      height: 0.1,
                      color: ColorService.fontPrimaryUnfocused,
                    )
                  ]
                : [],
          ],
        );
      },
    );
  }

  TextStyle _titleTextStyle(bool selected) => TextStyle(
        height: 1,
        fontSize: 15,
        color: adjustColorOfSelected && selected
            ? ColorService.accentPrimary
            : fontColor,
      );

  TextStyle _descriptionTextStyle(String descriptionString) {
    return TextStyle(
      height: 1,
      fontSize: details.description.indexOf(descriptionString) > 0 ? 9 : 11,
      color: ColorService.fontPrimaryUnfocused,
    );
  }
}
