import 'package:hive_flutter/adapters.dart';
import 'package:performance_indicator/performance_indicator.dart';

part 'content_recording.g.dart';

@HiveType(typeId: 221)
class ContentRecording extends HiveObject {
  ContentRecording({
    required this.contentId,
    required this.qualityPreference,
    required this.lastUpdate,
    this.notAccessible = false,
  });

  @HiveField(0)
  final String contentId;

  @HiveField(1)
  final QualityPreference qualityPreference;

  @HiveField(2)
  final int lastUpdate;

  @HiveField(3)
  final bool? notAccessible;

  static withProperty(
    ContentRecording contentRecording, {
    QualityPreference? qualityPreference,
    int? lastUpdate,
    bool? notAccessible,
  }) {
    return ContentRecording(
      contentId: contentRecording.contentId,
      notAccessible: notAccessible ?? contentRecording.notAccessible,
      qualityPreference:
          qualityPreference ?? contentRecording.qualityPreference,
      lastUpdate: lastUpdate ?? contentRecording.lastUpdate,
    );
  }
}
