import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:http/http.dart' as http;
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/model/speed_details/speed_details.dart';
import 'package:performance_indicator/src/repo/db/hive_db.dart';
import 'package:performance_indicator/src/services/performance_service.dart';
import 'package:performance_indicator/src/services/stopwatch_service.dart';

class SpeedService {
  static final SpeedService _singleton = SpeedService._internal();

  // ignore: unused_element
  factory SpeedService._() {
    return _singleton;
  }

  SpeedService._internal();

  static int badResponseTime = 500;
  static int badResponseTimePing = 100;
  static Uri checkUri = Uri(host: 'google.de', scheme: 'https');

  static HiveDB? hiveDb;

  static Future<SpeedService> instance() async {
    hiveDb = await HiveDB.instance();

    return _singleton;
  }

  SpeedDetails? getLastCheckedSpeed() {
    if (_db()?.values.isEmpty ?? true) {
      return null;
    }

    return _db()?.values.last;
  }

  Future<T?> checkCurrentSpeed<T>({
    Future<T>? request,
    String? url,
    int? badResponseTime,
    int? timeoutMs,
    bool withPing = false,
  }) async {
    final performanceService = await PerformanceService.instance();
    final selectedQuality = performanceService.getQualityPreference();
    final uri = url != null ? Uri.parse(url) : checkUri;

    final executable = request ??
        (http.get(
          uri,
          headers: {'Access-Control-Allow-Origin': '*'},
        ) as Future<T>);

    return await StopwatchService.async<T>(
      timeoutMs != null
          ? executable.timeout(Duration(milliseconds: timeoutMs))
          : executable,
      micro: true,
      log: true,
      name: uri.toString(),
      whenCompleted: (measured, {required bool isResultEmpty}) async {
        final uriTime =
            measured ?? badResponseTime ?? SpeedService.badResponseTime;

        if (isResultEmpty) {
          await updateCurrentSpeed(
            SpeedDetails.of(
              responseTimeUri: uriTime,
              responseTimePing: SpeedService.badResponseTimePing,
              checkUrl: uri.toString(),
              qualityPreference: selectedQuality ?? QualityPreference.high,
              badResponseTime: badResponseTime,
              isResultEmpty: true,
            ),
          );

          return;
        }

        int? pingTime;

        if (withPing) {
          pingTime = await pingGoogle(
            selectedQuality ?? QualityPreference.high,
          );
        }
        await updateCurrentSpeed(
          SpeedDetails.of(
            responseTimeUri: uriTime,
            responseTimePing: pingTime ?? (badResponseTimePing * 0.5).toInt(),
            checkUrl: uri.toString(),
            qualityPreference: selectedQuality ?? QualityPreference.high,
            badResponseTime: badResponseTime,
          ),
        );
      },
    );
  }

  Future<void> updateCurrentSpeed(SpeedDetails speedDetails) async {
    await _db()?.add(speedDetails);
  }

  Future<void> removeOldRecordings() async {
    final all = _db()?.values ?? [];

    final oldRecordings = all.where(_isOldRecording).toList();

    await Future.forEach(oldRecordings, (SpeedDetails oldRecording) async {
      await deleteRecording(oldRecording);
    });
  }

  Future<void> deleteRecording(SpeedDetails speedDetails) async {
    await _db()?.delete(speedDetails.key);
  }

  static double calcSpeedQuality({
    required int responseTimeUri,
    int? responseTimePing,
    int? badResponseTime,
  }) {
    double? speedQualityPing;

    if (responseTimePing != null) {
      speedQualityPing = SpeedService._calcPingQuality(responseTimePing);
    }

    final speedQualityUri = SpeedService._calcUriQuality(
      responseTimeUri,
      customBadResponseTime: badResponseTime,
    );

    return kIsWeb || speedQualityPing == null
        ? speedQualityUri
        : speedQualityUri * 0.6 + speedQualityPing * 0.4;
  }

  static isActionRequired(double speed, QualityPreference selectedQuality) {
    switch (selectedQuality) {
      case QualityPreference.low:
        {
          return speed < 0.25;
        }
      case QualityPreference.medium:
        {
          return speed < 0.4;
        }
      case QualityPreference.high:
        {
          return speed < 0.7;
        }
      default:
        return false;
    }
  }

  static setBadResponseTime(int newBadResponseTime) {
    badResponseTime = newBadResponseTime;
  }

  static setCheckUri(Uri uri) {
    checkUri = uri;
  }

  static const String dbName = 'speed_details';

  static Box<SpeedDetails>? _db() {
    return hiveDb?.dbWithType<SpeedDetails>(dbName);
  }

  /// Pings Google's 8.8.8.8:53 but not on Web!
  static Future<int> pingGoogle(QualityPreference selectedQuality) async {
    if (kIsWeb) return 10000;

    final Completer<int> completer = Completer();

    await StopwatchService.async(
      Socket.connect(
        '8.8.8.8',
        53,
        timeout: Duration(milliseconds: badResponseTimePing * 2),
      ),
      micro: true,
      log: true,
      name: 'PING',
      whenCompleted: (int? elapsed, {required bool isResultEmpty}) {
        completer.complete(elapsed ?? SpeedService.badResponseTimePing);
      },
    ).then((socket) async {
      socket?.destroy;

      return socket;
    }).onError((error, stacktrace) {
      completer.complete(badResponseTimePing);

      if (kDebugMode) {
        print('Exception on ping. Error:${error.toString()}');
      }

      return null;
    });

    return completer.future;
  }

  static double _calcUriQuality(
    int responseTime, {
    int? customBadResponseTime,
  }) {
    final badTimeInMicro = (customBadResponseTime ?? badResponseTime) * 1000;

    if (responseTime > badTimeInMicro) {
      return 0;
    } else {
      return 1 - (responseTime / badTimeInMicro);
    }
  }

  static double _calcPingQuality(int responseTime) {
    final badTimeInMicro = badResponseTimePing * 1000;

    if (responseTime > badTimeInMicro) {
      return 0;
    } else {
      return 1 - (responseTime / badTimeInMicro);
    }
  }

  bool _isOldRecording(SpeedDetails details) {
    final timestamp = DateTime.fromMillisecondsSinceEpoch(details.timestamp);
    final allowedTimestamp =
        DateTime.now().subtract(const Duration(minutes: 60));

    return timestamp.isBefore(allowedTimestamp);
  }
}
