import 'dart:async';

import 'package:hive_flutter/adapters.dart';
import 'package:performance_indicator/src/model/content_recording/content_recording.dart';
import 'package:performance_indicator/src/model/quality_preference/quality_preference.dart';
import 'package:performance_indicator/src/repo/db/hive_db.dart';

class ContentService {
  static final ContentService _singleton = ContentService._internal();

  // ignore: unused_element
  factory ContentService._() {
    return _singleton;
  }

  ContentService._internal();

  static HiveDB? hiveDb;

  static Future<ContentService> instance() async {
    hiveDb = await HiveDB.instance();

    return _singleton;
  }

  Future<ContentRecording> createContentRecording(
    ContentRecording contentRecording,
  ) async {
    final content = getByContentId(contentRecording.contentId);

    if (content != null && content.key != null) {
      await _db()?.put(content.key, contentRecording);
    } else {
      await _db()?.put(contentRecording.contentId, contentRecording);
    }

    return contentRecording;
  }

  ContentRecording? getByContentId(String contentId) {
    if (_db()?.values.isEmpty ?? true) {
      return null;
    }

    final result = _db()
        ?.values
        .where((element) => element.contentId == contentId)
        .toList();

    if (result?.isNotEmpty ?? false) {
      return result?.first;
    }

    return null;
  }

  Future<ContentRecording> updateQualityPreference(
    String contentId,
    QualityPreference qualityPreference,
  ) async {
    final content = getByContentId(contentId);

    late ContentRecording newContent;

    if (content != null) {
      newContent = ContentRecording.withProperty(
        content,
        qualityPreference: qualityPreference,
        lastUpdate: DateTime.now().millisecondsSinceEpoch,
      );

      await _db()?.put(content.key, newContent);

      return newContent;
    }

    newContent = ContentRecording(
      contentId: contentId,
      qualityPreference: qualityPreference,
      lastUpdate: DateTime.now().millisecondsSinceEpoch,
    );

    await createContentRecording(newContent);

    return newContent;
  }

  Future<bool> markContentAsNotAccessible(String contentId) async {
    final content = getByContentId(contentId);

    if (content != null) {
      final updatedContent = ContentRecording.withProperty(
        content,
        lastUpdate: DateTime.now().millisecondsSinceEpoch,
        notAccessible: true,
      );

      await _db()?.put(content.key, updatedContent);

      return true;
    }

    return false;
  }

  static const String dbName = 'content_recording';

  static Box<ContentRecording>? _db() {
    return hiveDb?.dbWithType<ContentRecording>(dbName);
  }
}
