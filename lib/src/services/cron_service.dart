import 'package:cron/cron.dart';
import 'package:flutter/foundation.dart';
import 'package:performance_indicator/src/services/speed_service.dart';

class CronService {
  static final CronService _singleton = CronService._internal();

  factory CronService() {
    return _singleton;
  }

  CronService._internal();

  List<String> scheduledTasks = [];

  Future<void> init() async {
    await Future.delayed(const Duration(milliseconds: 0));
    const String cronName = 'CRON_TASK_check_internet_connection';

    if (scheduledTasks.where((element) => element == cronName).isEmpty) {
      final manageBroadcastRoomLibCron = Cron();
      manageBroadcastRoomLibCron.schedule(
        Schedule.parse('* * * * *'),
        () async {
          if (kDebugMode) {
            print('Start cron: $cronName');
          }

          await checkInternetConnectionSpeed();
          await removeOldRecordings();
        },
      );

      scheduledTasks.add(cronName);

      if (kDebugMode) {
        print('Scheduled cron : $cronName');
      }
    }
  }

  Future<void> checkInternetConnectionSpeed() async {
    final speedService = await SpeedService.instance();

    await speedService.checkCurrentSpeed(
      timeoutMs: SpeedService.badResponseTime * 2,
    );
  }

  removeOldRecordings() async {
    final speedService = await SpeedService.instance();

    await speedService.removeOldRecordings();
  }
}
